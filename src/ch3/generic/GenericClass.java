package ch3.generic;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class GenericClass<M> {
	private M obj;

	public static void main(String[] args) {
		GenericClass<Integer> obj1 = new GenericClass<>();
		obj1.set(1);
		GenericClass<String> obj2 = new GenericClass<>();
		obj2.set("1");
	}

	public M get() {
		return obj;
	}

	public void set(M obj) {
		this.obj = obj;
	}
}
