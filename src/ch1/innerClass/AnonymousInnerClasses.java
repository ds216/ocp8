package ch1.innerClass;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 4/21/2019
 */
public class AnonymousInnerClasses {
	public int admission(int basePrice) {
		SaleTodayOnly sale = new SaleTodayOnly() {
			int dollarsOff() {
				return 3;
			}
		};
		return basePrice - sale.dollarsOff();
	}

	abstract class SaleTodayOnly {
		abstract int dollarsOff();
	}
}
