package ch3.generic;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class GenericMethod {
	public <T> int count(T obj) {
		return obj.hashCode();
	}

	public <T> void test() {
	}

	//public T void tes2(){}
}
