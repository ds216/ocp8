package ch2.immutable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class Modify {
	public static void main(String[] args) {
		Immutable obj = new Immutable(Arrays.asList("ABC", "DEF"), "test");

		List<String> list = new ArrayList<>();
		for (int i = 0; i < obj.getStringsCount(); i++) {
			list.add(obj.getString(i));
		}

		Immutable updated = new Immutable(list, obj.getName());
	}
}
