package ch2.interfaceTest;

interface A {
	default void test() {
		System.out.println("Test A");
	}
}

interface B {
	default void test() {
		System.out.println("Test B");
	}
}

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class Failed {
	public static void main(String[] args) {

	}
}
//interface C extends A,B{
//}