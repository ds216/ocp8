package ch1.instanceOf;


/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class Example {

	public static void main(String... args) {
		System.out.println(null instanceof Object); //False

		//Not Compiled, String and Integer not extends directly or indirectly
		//neither String extends Integer nor Integer extends String is true
		// System.out.print("abc" instanceof Integer);

		SubClass a = new SubClass();
		System.out.println(a instanceof SuperClass); // True

		SuperClass b = new SuperClass();
		System.out.println(b instanceof SubClass); //False, because SubClass extends SuperClass
	}


}

class SuperClass {
}

class SubClass extends SuperClass {
}