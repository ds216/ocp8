package ch1.vmi;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class Example2 {


	public static void main(String[] args) {
		Animal2 animal = new Human2();
		animal.printName();                         // print "Human"
		animal.test();                              // print "TEST B"
		System.out.println(animal.name);            // print "?"

		//Advanced
		animal.printName();                // print "Human"
		animal.test();                     // print "TEST B"
		System.out.println(((Human2) animal).name);   // print "Human"

		animal.printName();               // print "Human"
		animal.test();                    // print "TEST B"
		System.out.println(animal.name);  // print "?"
	}
}

class Human2 extends Animal2 {
	String name = "Human";

	public void printName() {
		System.out.println(name);
	}

	public void test() {
		System.out.println("Test B");
	}
}

abstract class Animal2 {
	String name = "?";

	public void printName() {
		System.out.println(name);
	}

	public void test() {
		System.out.println("Test A");
	}
}