package ch3.generic;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public interface GenericInterface<T> {
	void print(T obj);
}

class Test1<U> implements GenericInterface<U> {
	@Override
	public void print(U obj) {
	}
}

class Test2 implements GenericInterface {
	@Override
	public void print(Object obj) {
	}
}

class Test3 implements GenericInterface<String> {
	public static void main(String[] args) {
		GenericInterface obj = new Test3();
		obj.print("Test");
	}

	@Override
	public void print(String obj) {
		System.out.print(obj);
	}
}
