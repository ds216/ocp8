package ch2.singleton;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class Singleton2 {
	private static Singleton2 instance;

	static { //static initialization block
		instance = new Singleton2();
	}

	private Singleton2() {
	}

	public static Singleton2 getInstance() {
		return instance;
	}

	public static void main(String[] args) {
		Singleton2 obj = Singleton2.getInstance();
	}

}
