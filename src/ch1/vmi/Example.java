package ch1.vmi;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class Example {


	public static void main(String[] args) {
		Animal animal = new Human();
		animal.printName();                         // print "?"
		animal.test();                              // print "TEST B"
		System.out.println(animal.name);            // print "?"

		//Advanced
		animal.printName();                // print "?"
		animal.test();                     // print "TEST B"
		System.out.println(((Human) animal).name);   // print "Human"

		animal.printName();               // print "?"
		animal.test();                    // print "TEST B"
		System.out.println(animal.name);  // print "?"
	}
}

class Human extends Animal {
	String name = "Human";

	public void test() {
		System.out.println("Test B");
	}
}

abstract class Animal {
	String name = "?";

	public void printName() {
		System.out.println(name);
	}

	public void test() {
		System.out.println("Test A");
	}
}