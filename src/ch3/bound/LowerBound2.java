package ch3.bound;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class LowerBound2 {
	public static void main(String[] args) {
		List<? super IOException> exceptions = new ArrayList<Exception>();
		//exceptions.add(new Exception()); // DO NOT COMPILE
		exceptions.add(new IOException());
		exceptions.add(new FileNotFoundException());
	}
}
