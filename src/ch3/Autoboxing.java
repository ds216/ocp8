package ch3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 4/22/2019
 */
public class Autoboxing {
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();
		list.add(1); // AutoBoxing
		int a = list.get(0); // Unboxing
	}
}
