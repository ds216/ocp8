package ch1.enums;

enum Season {
	WINTER, SPRING, SUMMER, FALL
}

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class Example {
	public static void main(String[] args) {
		Season s = Season.SUMMER;
		System.out.println(s == Season.SUMMER);

		for (Season s1 : Season.values()) {
			System.out.println(s1.name() + "," + s1.ordinal());
		}

		Season s2 = Season.valueOf("SUMMER");
		System.out.println(s2);

		Season s3 = Season.valueOf("summer");
		System.out.println(s3);

		Season s4 = Season.SUMMER;
		switch (s4) {
			case WINTER:
			case SUMMER:
			case SPRING:
			case FALL:
			default:
		}
	}
}
