package ch2.singleton;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class Singleton3 {
	private static Singleton3 instance;

	private Singleton3() {
	}

	public static Singleton3 getInstance() {
		//Lazy Instantiation
		if (instance == null)
			instance = new Singleton3();
		return instance;
	}

	public static void main(String[] args) {
		Singleton3 obj = new Singleton3();
	}
}

