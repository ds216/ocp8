package ch1.enums;

enum Test2 {
	Test("TEST") {
		@Override
		public void print() { //implement the abstract method
			System.out.println("Test");
		}

		@Override
		public void print2() {
			System.out.println("Test");
		}
	};
	private String value;

	Test2(String value) {
		this.value = value;
	}

	public abstract void print();// need to implement in all enum

	public void print2() { // default method for all enum
	}

}

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 4/21/2019
 */
public class Example4 {
}
