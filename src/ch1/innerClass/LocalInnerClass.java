package ch1.innerClass;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 4/21/2019
 */
public class LocalInnerClass {
	private int length = 5;

	public static void main(String[] args) {
		LocalInnerClass outer = new LocalInnerClass();
		outer.calculate();
	}

	public void calculate() {
		final int width = 20;
		int num = 5;
		class Inner {
			public void multiply() {
				System.out.println(length * width);
				//System. out .println(length * width*num);
				//Error: local variables referenced from an inner class must be final or effectively final
			}
		}
		num++;
		Inner inner = new Inner();
		inner.multiply();
	}

	public void isItFinal() {
		int one = 20;
		int two = one;
		two++;
		int three;
		if (one == 4) three = 3;
		else three = 4;
		int four = 4;
		class Inner {
		}
		four = 5;
	}
}

