package ch3.bound;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class UpperBounded {
	public static void print(List<? extends Number> list) {
		//list.add(3); //DO NOT COMPILE
		System.out.println(list);
	}

	public static void main(String[] args) {
//		List<String> stringList = new ArrayList<>();
//		stringList.add("Test");
//		print(stringList); //DO NOT COMPILE
		List<Integer> integerList = new ArrayList<>();
		integerList.add(1);
		print(integerList);
	}
}
