package ch1.innerClass;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class MemberInnerClass2 {
	public static void main(String[] args) {
		A a = new A();
		A.B b = a.new B();
		A.B.C c = b.new C();
		c.allTheX();
	}
}

class A {
	private int x = 10;

	class B {
		private int x = 20;

		class C {
			private int x = 30;

			public void allTheX() {
				System.out.println(x); // 30, C.x
				System.out.println(this.x); // 30, C.x
				System.out.println(B.this.x); // 20, B.x
				System.out.println(A.this.x); // 10, A.x
			}
		}
	}
}