package ch2.polymorphic;

interface LivesInOcean {
	void makeSound();
}

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 4/22/2019
 */
public class Example1 {
	public static void main(String[] args) {
		Example1 o = new Example1();
		o.checkSound(new Dolphin());
		o.checkSound(new Whale());
	}

	public void checkSound(LivesInOcean animal) {
		animal.makeSound();
	}
}

class Dolphin implements LivesInOcean {
	public void makeSound() {
		System.out.println("whistle");
	}
}

class Whale implements LivesInOcean {
	public void makeSound() {
		System.out.println("sing");
	}
}