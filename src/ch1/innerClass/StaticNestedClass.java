package ch1.innerClass;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 4/21/2019
 */
public class StaticNestedClass {
	public static void main(String[] args) {
		Nested nested = new Nested();
		System.out.println(nested.price);//access private instance
	}

	static class Nested {
		private int price = 6;
	}
}
