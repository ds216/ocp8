package ch1.innerClass;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class MemberInnerClass {
	public static void main(String[] args) {
		Outer outer = new Outer();
		outer.callInner();

		Outer outer2 = new Outer();
		Outer.Inner inner = outer2.new Inner();
		inner.go();
	}
}

class Outer {
	private String string = "Hi";

	public void callInner() {
		Inner inner = new Inner();
		inner.go();
	}

	protected class Inner {
		public int repeat = 3;

		public void go() {
			for (int i = 0; i < repeat; i++)
				System.out.println(string);
			//Can access members of the outer class including private members
		}
	}
}