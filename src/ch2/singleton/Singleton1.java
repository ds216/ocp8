package ch2.singleton;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class Singleton1 {
	private static Singleton1 instance = new Singleton1(); //effectively final
	private int count = 0;

	private Singleton1() {
	} // not possible to create subclass

	public static Singleton1 getInstance() {
		return instance;
	}

	public static void main(String[] args) {
		Singleton1 obj = Singleton1.getInstance();
		obj.addCount(5);
		Singleton1 obj2 = Singleton1.getInstance();
		obj2.minusCount(3);
		System.out.println(Singleton1.getInstance().getCount());
	}

	public synchronized void addCount(int n) {
		count += n;
	}

	public synchronized void minusCount(int n) {
		count -= n;
	}

	public int getCount() {
		return count;
	}
}
