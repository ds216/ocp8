package ch2.polymorphic;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 4/22/2019
 */
public class Casting {
}

class Bird {
}

class Fish {
	public static void main(String[] args) {
		Fish fish = new Fish();
		//Bird bird = bird; // DOES NOT COMPILE
	}
}

class Rodent {
}

class Capybara extends Rodent {
	public static void main(String[] args) {
		Rodent rodent = new Rodent();
		Capybara capybara = (Capybara) rodent; // Throws ClassCastException at
// runtime
		if (rodent instanceof Capybara) {
			Capybara capybara2 = (Capybara) rodent;
		}
	}
}