package ch3.bound;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 4/22/2019
 */
public class Unbound {
	//	public static void print(List<Object> list){} // DO NOT COMPILE
	public static void print(List<?> list) {
	}

	public static void main(String[] args) {
		List<String> stringList = new ArrayList<>();
		stringList.add("Test");
		print(stringList);
		List<Integer> integerList = new ArrayList<>();
		integerList.add(1);
		print(integerList);
	}
}
