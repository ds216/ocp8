package ch2.polymorphic;

interface HasTail {
	boolean isTailStriped();
}

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class Example2 {
	public static void main(String[] args) {
		Lemur lemur = new Lemur();
		System.out.println(lemur.age);
		HasTail hasTail = lemur;
		System.out.println(hasTail.isTailStriped());
		//System.out.println(hasTail.age); // DOES NOT COMPILE
		Primate primate = lemur;
		System.out.println(primate.hasHair());
		//System.out.println(primate.isTailStriped()); // DOES NOT COMPILE
	}
}

class Primate {
	public boolean hasHair() {
		return true;
	}
}

class Lemur extends Primate implements HasTail {
	public int age = 10;

	public boolean isTailStriped() {
		return false;
	}
}