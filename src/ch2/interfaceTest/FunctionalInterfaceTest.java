package ch2.interfaceTest;

@FunctionalInterface
interface Sprint {
	void sprint(Animal animal);
}

@FunctionalInterface
interface Run extends Sprint {
}

@FunctionalInterface
interface SprintFaster extends Sprint {
	void sprint(Animal animal);
}

interface Skip extends Sprint {
	static void skip(int speed) {
	}

	default int getHopCount(Kangaroo kangaroo) {
		return 10;
	}
}

interface Walk {
}

interface Dance extends Sprint {
	void dance(Animal animal);
}

interface Crawl {
	void crawl();

	int getCount();
}

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 4/22/2019
 */
public class FunctionalInterfaceTest {

}

class Kangaroo extends Animal {
}

class Animal {
}
