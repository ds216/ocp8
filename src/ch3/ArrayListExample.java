package ch3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 4/22/2019
 */
public class ArrayListExample {
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("String1");
		list.add("String2");

		String[] arr = new String[list.size()];
		arr[0] = list.get(0);
		arr[1] = list.get(1);

		String[] array = {"01", "02"}; // [01, 02]
		List<String> list2 = Arrays.asList(array); // returns fixed size list
		list2.set(1, "03"); // [01, 03]
		array[0] = "04"; // [04, 03]
		String[] array2 = (String[]) list2.toArray(); // [04, 03]
		list.remove(1); //UnsupportedOperationException

		int[] numbers = {6, 9, 1, 8};
		Arrays.sort(numbers); // [1,6,8,9]
		System.out.println(Arrays.binarySearch(numbers, 6)); // 1
		System.out.println(Arrays.binarySearch(numbers, 3)); // -2

		List<Integer> list3 = Arrays.asList(9, 7, 5, 3);
		Collections.sort(list); // [3, 5, 7, 9]
		System.out.println(Collections.binarySearch(list3, 3)); // 0
		System.out.println(Collections.binarySearch(list3, 2)); // -1
	}
}
