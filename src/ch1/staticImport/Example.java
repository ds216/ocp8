package ch1.staticImport;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Math.sqrt;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public class Example {

	public static void main(String... args) {
		//Math.sqrt()
		System.out.println(sqrt(9));

		//Integer.MAX_VALUE
		System.out.println(MAX_VALUE);
	}
}
