package ch2.immutable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 */
public final class Immutable {
	private final List<String> strings;
	private final String name;

	public Immutable(List<String> strings, String name) {
		// Prevent the List can be modify outside this class
		this.strings = new ArrayList<>(strings);
		this.name = name;
	}

	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("ABC");
		Immutable obj = new Immutable(list, "test");
		list.add("DEF");
	}

	public List<String> getStrings() {
		//This method should be removed as it makes the class mutable.
		return strings;
	}

	public int getStringsCount() {
		return strings.size();
	}

	public String getString(int index) {
		return strings.get(index);
	}

	public String getName() {
		return name;// String is Immutable
	}
}
